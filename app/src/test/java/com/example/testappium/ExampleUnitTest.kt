package com.example.testappium

import org.junit.Test
import io.appium.java_client.MobileCommand
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.remote.MobileCapabilityType

import org.junit.Assert.*
import org.junit.Before
import org.openqa.selenium.By
import org.openqa.selenium.remote.DesiredCapabilities
import java.net.URL

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private lateinit var driver: AndroidDriver

    @Before
    fun setup(){
        val capabilities = DesiredCapabilities()
        capabilities.setCapability("platformName", "Android")
        capabilities.setCapability("appium:automationName", "UiAutomator2")
        capabilities.setCapability("appium:deviceName", "emulator-5554")
        capabilities.setCapability("appium:app", "/home/analistakarla/Documentos/app/debug.apk")
        capabilities.setCapability("appium:noReset", true)
        capabilities.setCapability("appium:appWaitActivity", "com.example.testappium*")

        driver = AndroidDriver(URL("http://127.0.0.1:4723"), capabilities)

    }

    @Test
    fun viewMessage() {
        println("Iniciando prueba de inicio de sesión")
        driver.findElement(By.id("com.example.testappium:id/Email")).sendKeys("karlavillegas43@gmail.com")
        driver.findElement(By.id("com.example.testappium:id/Password")).sendKeys("12345678")
        driver.findElement(By.id("com.example.testappium:id/button")).click()


       /* assertEquals(4, 2 + 2)*/
    }
}